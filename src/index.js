import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'

import '@here/lui-init/dist/lui-init.min'
import '@here/lui-webfont/dist/lui-webfont.min'
import '@here/lui-layer-theme/dist/lui-layer-theme.min'
import '@here/lui-button/dist/lui-button.min'
import '@here/lui-navigation/dist/lui-navigation.min'
import '@here/lui-tabbar/dist/lui-tabbar.min'
import '@here/lui-checkbox/dist/lui-checkbox.min'
import '@here/lui-logo/dist/lui-logo.min'
import '@here/lui-spinner/dist/lui-spinner.min'
import '@here/lui-search/dist/lui-search.min'
import '@here/lui-menu/dist/lui-menu.min'
import '@here/lui-label/dist/lui-label.min'
import '@here/lui-radiobutton/dist/lui-radiobutton.min'
import '@here/lui-overlay/dist/lui-overlay.min'
import '@here/lui-notification/dist/lui-notification.min'
import '@here/lui-modal/dist/lui-modal.min'

import './assets/css/index.css'

ReactDOM.render(
  <App />,
  document.getElementById('root')
)
