import React, { useState } from 'react'
import { Analytics } from './Analytics'
import { Routing } from './Routing'
import PropTypes from 'prop-types'

import svg from './icons/LUI-icon-pd-chevron_left_double-solid-24.svg'

import './style.css'
  
export const Sidebar = props => { 

    const [show, setVisibility] = useState(true);

    let { 
        analytics,
        changeActiveLayer,
        changeMode,
        options,
        dropdownClick,
        onChangeIsolineRange,
        clickRadioButtonParameter,
    } = props

    return (
        <div className={show ? "sidebar" : "sidebar-collapse"}>
            <lui-navigation>
                <lui-productlogo slot="productlogo">Healthcare Resources</lui-productlogo>
                <lui-label slot="controls" onClick={() => setVisibility(!show)} className="lui-small">
                        { 
                            show === true? 
                                <slot name="icon">
                                    <img className="chevron-down" src={svg}></img>
                                </slot>
                            :
                                <slot name="icon">
                                    <img className="chevron-left" src={svg}></img>
                                </slot>
                        }
                </lui-label>
                <lui-label slot="controls-mobile" onClick={() => setVisibility(!show)} className="lui-small">
                        { 
                            show === true? 
                                <slot name="icon">
                                    <img className="chevron-down" src={svg}></img>
                                </slot>
                            :
                                <slot name="icon">
                                    <img className="chevron-left" src={svg}></img>
                                </slot>
                        }
                </lui-label>
            </lui-navigation>
            <div className={show ? "d-block" : "d-none"}>
                <div className="switch-options">
                    <div 
                        onClick={ () => changeMode("analytics") } 
                        className={analytics.mode === "analytics" ? "tab-btn tab-active" : "tab-btn"}>Tracking</div>
                    <div 
                        onClick={ () => changeMode("routing") } 
                        className={analytics.mode === "routing" ? "tab-btn tab-active" : "tab-btn"}>Routing</div>
                </div>
                
                {   
                    analytics.mode === "analytics" ? 
                        <Analytics 
                            analytics={analytics}
                            changeActiveLayer={changeActiveLayer}
                            dropdownClick={dropdownClick} />
                    :

                        <Routing 
                            options={options}
                            clickRadioButtonParameter={clickRadioButtonParameter}
                            onChangeIsolineRange={onChangeIsolineRange}
                            dropdownClick={dropdownClick}/>
                }
            
            </div>
        </div>   
    )
}

Sidebar.propTypes = {
    analytics: PropTypes.object,
    options: PropTypes.object,
    changeActiveLayer: PropTypes.func,
    changeMode: PropTypes.func,
    dropdownClick: PropTypes.func,
    onChangeIsolineRange: PropTypes.func,
    clickRadioButtonParameter: PropTypes.func,
}