import React, { Component } from 'react'
import PropTypes from 'prop-types'

import chevronDown from './icons/LUI-icon-pd-chevron_down-solid-16.svg'
import './style.css'


export const Analytics = ({analytics, changeActiveLayer, dropdownClick}) => {

    return (
        <div className="parameters">
            
            <lui-label>Hospitals status</lui-label>
            
            {
                analytics.hospitals !== null ? 
                    <lui-radiobutton-group>
                        
                        {/* ICU low care */}

                        <div className="analytics-parameter-block">
                            <div className="analytics-item-row">
                                <div className="analytics-item-column">
                                    {
                                        analytics.dropdown.low_care ?
                                            <img className="chevron-down-icon" onClick={() => dropdownClick("low_care")} src={chevronDown} ></img>
                                        :
                                            <img className="chevron-right-icon" onClick={() => dropdownClick("low_care")} src={chevronDown} ></img>
                                    }
                                </div>
                                {
                                    analytics.active_layer === "low_care" ?
                                        <lui-radiobutton checked > ICU low care </lui-radiobutton>
                                    :
                                        <lui-radiobutton onClick={() => changeActiveLayer("low_care")}> ICU low care </lui-radiobutton>
                                }
                            </div>
                            <div className={analytics.dropdown.low_care ? "analytics-item-row" : "d-none"}>
                                <div className="analytics-item-column">
                                    <span className="analytics-item-description">
                                        Intensive care beds without invasive ventilation (monitoring, possibly non-invasive ventilation possible)
                                    </span>
                                    <div className="analytics-item-legend">
                                        <span className="legend-item">
                                            <div className="legend-marker-green"></div>
                                            Avalilable: {analytics.statistics.low_care.available}
                                        </span>
                                        <span className="legend-item">
                                            <div className="legend-marker-yellow"></div>
                                            Limited: {analytics.statistics.low_care.limited}
                                        </span>
                                        <span className="legend-item">
                                            <div className="legend-marker-red"></div>
                                            Not avalilable: {analytics.statistics.low_care.not}
                                        </span>
                                        <span className="legend-item">
                                            <div className="legend-marker-purple"></div>
                                            No data: {analytics.statistics.low_care.nodata}
                                        </span>
                                    </div>                            
                                </div>
                            </div>
                        </div>


                        {/* ICU high care */}
                        <div className="analytics-parameter-block">
                            <div className="analytics-item-row">
                                <div className="analytics-item-column">
                                    {
                                        analytics.dropdown.high_care ?
                                            <img className="chevron-down-icon" onClick={() => dropdownClick("high_care")}  src={chevronDown} ></img>
                                        :
                                            <img className="chevron-right-icon" onClick={() => dropdownClick("high_care")} src={chevronDown} ></img>
                                    }
                                </div>
                                {
                                    analytics.active_layer === "high_care" ?
                                        <lui-radiobutton checked> ICU high care </lui-radiobutton>
                                    :
                                        <lui-radiobutton onClick={() => changeActiveLayer("high_care")}> ICU high care </lui-radiobutton>
                                }
                            </div>
                            <div className={analytics.dropdown.high_care ? "analytics-item-row" : "d-none"}>
                                <div className="analytics-item-column">
                                    <span className="analytics-item-description">
                                        Intensive care beds with invasive ventilation (ventilation beds)
                                    </span>
                                    <div className="analytics-item-legend">
                                        <span className="legend-item">
                                            <div className="legend-marker-green"></div>
                                            Avalilable: {analytics.statistics.high_care.available}
                                        </span>
                                        <span className="legend-item">
                                            <div className="legend-marker-yellow"></div>
                                            Limited: {analytics.statistics.high_care.limited}
                                        </span>
                                        <span className="legend-item">
                                            <div className="legend-marker-red"></div>
                                            Not avalilable: {analytics.statistics.high_care.not}
                                        </span>
                                        <span className="legend-item">
                                            <div className="legend-marker-purple"></div>
                                            No data: {analytics.statistics.high_care.nodata}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>


                        {/* ECMO */}
                        <div className="analytics-parameter-block">
                            <div className="analytics-item-row">
                                <div className="analytics-item-column">
                                    {
                                        analytics.dropdown.ecmo ?
                                            <img className="chevron-down-icon" onClick={() => dropdownClick("ecmo")} src={chevronDown} ></img>
                                        :
                                            <img className="chevron-right-icon" onClick={() => dropdownClick("ecmo")} src={chevronDown} ></img>
                                    }
                                </div>
                                {
                                    analytics.active_layer === "ecmo" ?
                                        <lui-radiobutton checked> ECMO </lui-radiobutton>
                                    :
                                        <lui-radiobutton onClick={() => changeActiveLayer("ecmo")}> ECMO </lui-radiobutton>
                                }
                                
                            </div>
                            <div className={analytics.dropdown.ecmo ? "analytics-item-row" : "d-none"}>
                                <div className="analytics-item-column">
                                    <span className="analytics-item-description">
                                        Additional extracorporeal membrane oxygenation
                                    </span>
                                    <div className="analytics-item-legend">
                                        <span className="legend-item">
                                            <div className="legend-marker-green"></div>
                                            Avalilable: {analytics.statistics.ecmo.available}
                                        </span>
                                        <span className="legend-item">
                                            <div className="legend-marker-yellow"></div>
                                            Limited: {analytics.statistics.ecmo.limited}
                                        </span>
                                        <span className="legend-item">
                                            <div className="legend-marker-red"></div>
                                            Not avalilable: {analytics.statistics.ecmo.not}
                                        </span>
                                        <span className="legend-item">
                                            <div className="legend-marker-purple"></div>
                                            No data: {analytics.statistics.ecmo.nodata}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </lui-radiobutton-group>

                :
            
                    <div className="spinner">
                        <lui-spinner class="lui-big"></lui-spinner>
                    </div>
            }

        </div>
    )
}

Analytics.propTypes = {
    analytics: PropTypes.object,
    changeActiveLayer: PropTypes.func,
    dropdownClick: PropTypes.func
}